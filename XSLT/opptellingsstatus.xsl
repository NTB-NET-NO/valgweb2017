<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="no"/>

  <xsl:param name="FylkeNr">00</xsl:param>
  <xsl:param name="Valgtype">K</xsl:param>

  <xsl:param name="xmlpath">valg-xml-inn</xsl:param>
  <xsl:param name="xmlpathkommune">..\<xsl:value-of select="$xmlpath"/>\<xsl:value-of select="$Valgtype"/>02-</xsl:param>

  <xsl:template match ="/respons">
    <xsl:choose>
      <xsl:when test="$FylkeNr != '00'">
        <xsl:apply-templates select="rapport[data[@navn = 'FylkeNr'] = $FylkeNr ]"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  

  <xsl:template match="rapport">
    <h2>
      <xsl:value-of select="data[@navn = 'FylkeNavn']"/>
    </h2>
  <table width="800" cellpadding="2" cellspacing="0" border="1">
    <tr>
      <th style="font-size: 1em;" >Kommune</th>
      <th style="font-size: 1em;" >Siste rapporttidspunkt</th>
      <th style="font-size: 1em;" >Status</th>
    </tr>

    <xsl:apply-templates select="tabell/liste" />

  </table>

</xsl:template>


  <xsl:template match="tabell/liste">
    <xsl:variable name ="R02path">
      <xsl:value-of select ="$xmlpathkommune"/>
      <xsl:value-of select="data[@navn = 'KommNr']"/>
      <xsl:text>.xml</xsl:text>
    </xsl:variable>
    <xsl:variable name ="timestamp">
      <xsl:value-of select="document($R02path)/respons/rapport/data[@navn = 'SisteRegDato']"/>
      <xsl:text> - </xsl:text>
      <xsl:value-of select="document($R02path)/respons/rapport/data[@navn = 'SisteRegTid']"/>
    </xsl:variable>
    <xsl:if test="$timestamp != ' - '">
    <tr>
      <td style="font-size: 1em;" >
        <xsl:value-of select="data[@navn = 'KommNr']"/> - 
        <xsl:value-of select="data[@navn = 'KommNavn']"/>
      </td>
      <td style="font-size: 1em;" >
        <xsl:value-of select="$timestamp"/>
      </td>
      <td style="font-size: 1em;" >
        <xsl:value-of select="data[@navn = 'StatusInd']"/>
        <xsl:text> - </xsl:text>
        <xsl:call-template name="status_text">
          <xsl:with-param name="status">
            <xsl:value-of select="data[@navn = 'StatusInd']"/></xsl:with-param>
        </xsl:call-template>
      </td>
    </tr>
    </xsl:if>
  </xsl:template>

  <xsl:template name="status_text">
    <xsl:param name="status"/>

    <!--
    Statusindikator for innrapportering har følgende verdier
    0.	Ingen resultater innsendt
    1.	Bare foreløpige forhåndsstemmer innsendt
    2.	Endelige fhst. innsendt
    3.	Bare foreløpige valgtingsstemmer innsendt
    4.	Endelige vtst. innsendt
    5.	Foreløpige fhst. og foreløpige vtst. innsendt
    6.	Endelige fhst. og foreløpige vtst. innsendt
    7.	Foreløpige fhst. og endelige vtst. innsendt
    8.	Endelige fhst. og endelige vtst. Innsendt
    -->

    <xsl:choose>
      <xsl:when test="$status = 0">
        <xsl:text>Resultater mangler for en eller flere kretser i kommunen</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 1">
        <xsl:text>Bare foreløpige forhåndsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 2">
        <xsl:text>Bare endelige forhåndsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 3">
        <xsl:text>Bare foreløpige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 4">
        <xsl:text>Bare endelige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 5">
        <xsl:text>Foreløpige forhåndsstemmer og foreløpige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 6">
        <xsl:text>Endelige forhåndsstemmer og foreløpige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 7">
        <xsl:text>Foreløpige forhåndsstemmer og endelige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 8">
        <xsl:text>Endelige forhåndsstemmer og endelige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:otherwise>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



</xsl:stylesheet>