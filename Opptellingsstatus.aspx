<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Opptellingsstatus.aspx.vb" Inherits="ValgWeb2017.Opptellingsstatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Opptellingsstatus</title>
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
</head>
<body>
    <form id="mainForm" runat="server">

    <h1>Opptellingsstatus Kommunestyrevalg - Siste rapporteringstidspunkter</h1>
    <p>Inneholder bare kommuner som har rapportert inn resultater.</p>
    <p>Merk: I kommuner med kretsvis rapportering settes status etter den kretsen som er kommet kortest. 
Det betyr at en kommune kan ha status 0 selv om en vesentlig del av stemmene allerede er talt opp.</p>

    Velg fylke:
    <asp:DropDownList AutoPostBack="true" runat="server" ID="selector">
        <asp:ListItem Text="Alle" Value="00"></asp:ListItem> 
        <asp:ListItem Text="�stfold" Value="01"></asp:ListItem> 
        <asp:ListItem Text="Akershus" Value="02"></asp:ListItem> 
        <asp:ListItem Text="Oslo" Value="03"></asp:ListItem> 
        <asp:ListItem Text="Hedmark" Value="04"></asp:ListItem> 
        <asp:ListItem Text="Oppland" Value="05"></asp:ListItem> 
        <asp:ListItem Text="Buskerud" Value="06"></asp:ListItem> 
        <asp:ListItem Text="Vestfold" Value="07"></asp:ListItem> 
        <asp:ListItem Text="Telemark" Value="08"></asp:ListItem> 
        <asp:ListItem Text="Aust-Agder" Value="09"></asp:ListItem> 
        <asp:ListItem Text="Vest-Agder" Value="10"></asp:ListItem> 
        <asp:ListItem Text="Rogaland" Value="11"></asp:ListItem> 
        <asp:ListItem Text="Hordaland" Value="12"></asp:ListItem> 
        <asp:ListItem Text="Sogn og Fjordane" Value="14"></asp:ListItem> 
        <asp:ListItem Text="M�re og Romsdal" Value="15"></asp:ListItem> 
        <asp:ListItem Text="S�r-Tr�ndelag" Value="16"></asp:ListItem> 
        <asp:ListItem Text="Nord-Tr�ndelag" Value="17"></asp:ListItem> 
        <asp:ListItem Text="Nordland" Value="18"></asp:ListItem> 
        <asp:ListItem Text="Troms" Value="19"></asp:ListItem> 
        <asp:ListItem Text="Finnmark" Value="20"></asp:ListItem> 
    </asp:DropDownList>

    <asp:Label runat="server" ID="link"></asp:Label>

    <asp:Xml ID="xmlTransform" runat="server" TransformSource="XSLT/opptellingsstatus.xsl" DocumentSource="valg-xml-inn/K01.xml"></asp:Xml>

    </form>
</body>
</html>
