<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes"/>

<xsl:template match="/">
	<TABLE BORDER="1" BGCOLOR="#ffffff" CELLSPACING="0" CELLPADDING="3">
		<xsl:call-template name="tablehead"/>
		<xsl:apply-templates select="dataroot/spPartiliste">
			<xsl:sort order="descending" select="offisiellt"/>
			<xsl:sort select="forkortning"/>
		</xsl:apply-templates>
		
	</TABLE>
</xsl:template>

<xsl:template match="dataroot/spPartiliste">
	<tr>
	<td><xsl:value-of select="forkortning"/></td>
	<td><xsl:value-of select="navn"/></td>
<!--
	<td><xsl:if test="kommunenavn != ''"><xsl:value-of select="kommunenavn"/></xsl:if>
		<xsl:if test="fylkesnavn != ''"><xsl:value-of select="fylkesnavn"/> fylke</xsl:if>&#160;</td>
	<td align="center"><xsl:value-of select="fylkesnr_kommunenr"/>&#160;</td>
	<td>
		<xsl:choose>
			<xsl:when test="offisiellt='1'">Offisiellt</xsl:when>
			<xsl:when test="fellesliste='1'">Fellesliste</xsl:when>
			<xsl:otherwise>Liste</xsl:otherwise>
		</xsl:choose>&#160;</td>
-->		
	</tr>
</xsl:template>

<xsl:template name="tablehead">
	<TR>
	<TH style="width: 2cm">
	<xsl:text>Partikode</xsl:text>
	</TH>
	<TH style="width: 8cm">
	<xsl:text>Partinavn</xsl:text>
	</TH>
<!--
	<TH style="width: 4cm">
	<xsl:text>Stiller i</xsl:text>
	</TH>
	<TH style="width: 2cm">
	<xsl:text>Kommunenr.</xsl:text>
	</TH>
	<TH style="width: 1cm">
	<xsl:text>Partitype</xsl:text>
	</TH>
-->	
	</TR>
</xsl:template>

<xsl:template match="spPartiliste">

</xsl:template>

</xsl:stylesheet>